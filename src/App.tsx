import React from 'react';
import './App.css';
import {Provider} from 'react-redux';
import {ConfigStore} from './redux/ConfigStore';
import 'reactstrap';
import {BrowserRouter} from 'react-router-dom';
import Main from "./components/Main";

const store = ConfigStore();

function App() {
    return (
        <Provider store={store}>
            <BrowserRouter>
                <div className="App">
                    <Main/>
                </div>
            </BrowserRouter>
        </Provider>
    );
}

export default App;
