import React from "react";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faInstagramSquare, faLinkedin, faTwitterSquare} from "@fortawesome/free-brands-svg-icons";
import {faEnvelope} from "@fortawesome/free-solid-svg-icons";

function AboutUs() {

    return (
        <div className="container align-self-center">
            <h4 className="font-weight-light mt-3 mb-5">Welcome to Food Trucks</h4>
            <div className="row">
                <div className="col-12 col-md-4">
                    <p className="font-weight-light text-justify">
                        Food Trucks is a <span className="font-weight-bold">web development exercise</span>, which
                        consist in load Food Trucks information from <a
                        href="https://data.sfgov.org/Permitting/Mobile-Food-Facility-Permit/rqzj-sfat">data.sfgov.org</a>.
                        Then using Google Maps Javascript API, well, using it implementation on the library <a
                        href="https://www.npmjs.com/package/@react-google-maps/api">React-Google-Maps</a>. The
                        geological information from each food truck is render in the map as a <span
                        className="font-weight-bold">marker</span> with a custom <span
                        className="font-weight-bold">icon</span>. Also if the user allow it, his or her location
                        will be render in the map. When any <span className="font-weight-bold">marker</span> is
                        clicked, the map will render a window with further information regarding the food truck.
                    </p>
                </div>
                <div className="col-12 col-md-4">
                    <p className="font-weight-light text-justify">
                        This web site was build with <a href="https://reactjs.org">React JS</a>. Implementing <a
                        href="https://redux.js.org">Redux</a> architecture.
                    </p>
                    <img className="App-logo" src="logo192.png" height="10vmin" alt="react-logo"/>
                    <p className="font-weight-light text-justify">
                        The visual components were provide by <a href="https://getbootstrap.com">Bootstrap</a> and
                        it's implementation on the library <a href="https://reactstrap.github.io">Reactstrap</a>.
                    </p>
                </div>
                <div className="col-12 col-md-4">
                    <div className="card col-6">
                        <div className="image-cropper">
                            <img src="/images/IMG_9606.png" alt="developer"/>
                        </div>
                        <span className="font-weight-bold">Develop by:</span>
                        <p className="font-weight-light">Edgar Lora Ariza</p>
                        <div className="text-center">
                            <a className="btn btn-social-icon btn-linkedin"
                               href="https://www.linkedin.com/in/edgarloraariza/?locale=en_US" target="_blank">
                                <FontAwesomeIcon icon={faLinkedin}/>
                            </a>
                            <a className="btn btn-social-icon btn-twitter" href="https://twitter.com/edgarloraariza"
                               target="_blank">
                                <FontAwesomeIcon icon={faTwitterSquare}/>
                            </a>
                            <a className="btn btn-social-icon btn-twitter"
                               href="https://www.instagram.com/edgarloraariza/" target="_blank">
                                <FontAwesomeIcon icon={faInstagramSquare}/>
                            </a>
                            <a className="btn btn-social-icon" href="mailto:edgar.lora.ariza@gmail.com">
                                <FontAwesomeIcon icon={faEnvelope}/>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default AboutUs;