import React from "react";
import MapContainer from "./MapContainer";
import {UserLocationReducerState} from "../redux/UserLocationReducer";
import FoodTruck from "./Model/FoodTruck";

interface HomeProps {
    userLocation: UserLocationReducerState,
    foodTrucks: Array<FoodTruck>
}

function Home(props: HomeProps) {
    return (
        <div className="container content align-self-center">
            <div className="row mt-3">
                <div className="col-12 col-md-6 align-self-center">
                    <h4 className="font-weight-light">We know you like to try new things, here we track all the best Food Trucks in the area, so you can enjoy the best food to go!</h4>
                </div>
                <div className="col-12 col-md-6">
                    <img src="/images/food-truck.png" width="334" height="226" alt="logo"/>
                </div>
            </div>
            <div className="row mt-3">
                <div className="col-12">
                    <h4 className="font-weight-light">Here you can find the Best Food Trucks in the area!</h4>
                </div>
            </div>
            <div className="row mt-3">
                <div className="col-12">
                    <MapContainer userLocation={props.userLocation} foodTrucks={props.foodTrucks}/>
                </div>
            </div>
        </div>
    );
}

export default Home;