import React, {Component} from "react";
import Header from "./Header";
import {Redirect, Route, RouterProps, Switch, withRouter} from "react-router";
import {connect} from "react-redux";
import Home from "./Home";
import AboutUs from "./AboutUs";
import {fetchFoodTrucks, fetchUserLocation} from "../redux/ActionCreator";
import {UserLocationReducerState} from "../redux/UserLocationReducer";
import {ThunkDispatch} from "redux-thunk";
import {FoodTrucksReducerState} from "../redux/FoodTrucksReducer";
import Footer from "./Footer";


interface MainState {
    userLocation: UserLocationReducerState,
    foodTrucks: FoodTrucksReducerState
}

interface MainProps extends RouterProps {
    userLocation: UserLocationReducerState,
    foodTrucks: FoodTrucksReducerState,
    getUserLocation: Function,
    fetchFoodTrucksInformation: Function
}

const mapStateToProps = (state: MainState) => {
    return {
        userLocation: state.userLocation,
        foodTrucks: state.foodTrucks
    }
}

const mapDispatchToProps = (dispatch: ThunkDispatch<any, any, any>) => ({
    getUserLocation: () => {
        dispatch(fetchUserLocation());
    },
    fetchFoodTrucksInformation: () => {
        dispatch(fetchFoodTrucks());
    }
})

class Main extends Component<MainProps, MainState> {

    componentDidMount() {
        this.props.getUserLocation();
        this.props.fetchFoodTrucksInformation();
    }

    render() {
        return (
            <div>
                <Header/>
                <Switch>
                    <Route path="/home" component={() => <Home userLocation={this.props.userLocation} foodTrucks={this.props.foodTrucks.foodTrucks}/>}/>
                    <Route path="/aboutus" component={() => <AboutUs/>}/>
                    <Redirect to="/home"/>
                </Switch>
                <Footer/>
            </div>
        );
    }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Main));