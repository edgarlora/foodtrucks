export default class FoodTruck {
    objectid: number;
    applicant: string;
    facilitytype: string;
    cnn: number;
    locationdescription: string;
    address: string;
    blocklot: number;
    block: number;
    lot: number;
    permit: string;
    status: string;
    fooditems: string;
    x: number;
    y: number;
    latitude: number;
    longitude: number;
    schedule: string;
    received: string;
    priorpermit: number;
    expirationdate: string;
    location: {
        type: string,
        coordinates: number[]
    };
    ":@computed_region_bh8s_q3mv": number;
    ":@computed_region_fyvs_ahh9": number;
    ":@computed_region_p5aj_wyqh": number;
    ":@computed_region_rxqg_mtj9": number;
    ":@computed_region_yftq_j783": number;


    constructor(objectid: number, applicant: string, facilitytype: string, cnn: number, locationdescription: string, address: string, blocklot: number, block: number, lot: number, permit: string, status: string, fooditems: string, x: number, y: number, latitude: number, longitude: number, schedule: string, received: string, priorpermit: number, expirationdate: string, location: { type: string; coordinates: number[] }, __computed_region_bh8s_q3mv: number, __computed_region_fyvs_ahh9: number, __computed_region_p5aj_wyqh: number, __computed_region_rxqg_mtj9: number, __computed_region_yftq_j783: number) {
        this.objectid = objectid;
        this.applicant = applicant;
        this.facilitytype = facilitytype;
        this.cnn = cnn;
        this.locationdescription = locationdescription;
        this.address = address;
        this.blocklot = blocklot;
        this.block = block;
        this.lot = lot;
        this.permit = permit;
        this.status = status;
        this.fooditems = fooditems;
        this.x = x;
        this.y = y;
        this.latitude = latitude;
        this.longitude = longitude;
        this.schedule = schedule;
        this.received = received;
        this.priorpermit = priorpermit;
        this.expirationdate = expirationdate;
        this.location = location;
        this[":@computed_region_bh8s_q3mv"] = __computed_region_bh8s_q3mv;
        this[":@computed_region_fyvs_ahh9"] = __computed_region_fyvs_ahh9;
        this[":@computed_region_p5aj_wyqh"] = __computed_region_p5aj_wyqh;
        this[":@computed_region_rxqg_mtj9"] = __computed_region_rxqg_mtj9;
        this[":@computed_region_yftq_j783"] = __computed_region_yftq_j783;
    }
}