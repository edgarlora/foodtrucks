import React from "react";
import {Link} from "react-router-dom";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faEnvelope, faPhone} from "@fortawesome/free-solid-svg-icons";
import {
    faFacebookSquare,
    faGooglePlus,
    faLinkedin,
    faTwitterSquare,
    faYoutubeSquare
} from "@fortawesome/free-brands-svg-icons";

function Footer() {
    return (
        <div className="footer bg-dark mt-5 pt-4">
            <div className="container">
                <div className="row">
                    <div className="col-12 col-md-4">
                        <h5 className="text-light">Links</h5>
                        <ul className="list-unstyled">
                            <li><Link className="text-light" to="/home">Home</Link></li>
                            <li><Link className="text-light" to="/aboutus">About</Link></li>
                        </ul>
                    </div>
                    <div className="col-12 col-md-4 text-light">
                        <h5>Our Address</h5>
                        <address>
                            1670 NW 82 Avenue<br/>
                            Doral, Florida 33191<br/>
                            U.S<br/>
                            <FontAwesomeIcon icon={faPhone}/> : +(305) 779 8989<br/>
                            <FontAwesomeIcon icon={faEnvelope}/> : <a className="text-light"
                                                                      href="mailto:contactus@foodtrucks.com">
                            contactus@foodtrucks.com</a>
                        </address>
                    </div>
                    <div className="col-12 col-md-4 align-self-center">
                        <div className="text-center">
                            <a className="btn btn-social-icon btn-google" href="http://google.com/+">
                                <FontAwesomeIcon className="text-light" icon={faGooglePlus}/>
                            </a>
                            <a className="btn btn-social-icon btn-facebook"
                               href="http://www.facebook.com/profile.php?id=">
                                <FontAwesomeIcon className="text-light" icon={faFacebookSquare}/>
                            </a>
                            <a className="btn btn-social-icon btn-linkedin" href="http://www.linkedin.com/in/">
                                <FontAwesomeIcon className="text-light" icon={faLinkedin}/>
                            </a>
                            <a className="btn btn-social-icon btn-twitter" href="http://twitter.com/">
                                <FontAwesomeIcon className="text-light" icon={faTwitterSquare}/>
                            </a>
                            <a className="btn btn-social-icon btn-google" href="http://youtube.com/">
                                <FontAwesomeIcon className="text-light" icon={faYoutubeSquare}/>
                            </a>
                            <a className="btn btn-social-icon" href="mailto:contactus@foodtrucks.com">
                                <FontAwesomeIcon className="text-light" icon={faEnvelope}/>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Footer;