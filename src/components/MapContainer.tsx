import React from "react";
import {UserLocationReducerState} from "../redux/UserLocationReducer";
import {GoogleMap, InfoWindow, Marker, useLoadScript} from "@react-google-maps/api";
import FoodTruck from "./Model/FoodTruck";

interface MapContainerProps {
    userLocation: UserLocationReducerState,
    foodTrucks: Array<FoodTruck>
}

const mapContainerStyle = {
    width: 400,
    height: 400,
    paddingLeft: 'auto',
    paddingRight: 'auto'
}

const options = {
    disableDefaultUI: true,
    zoomControl: true
}


function MapContainer(props: MapContainerProps) {

    const [selected, setSelected] = React.useState<FoodTruck>();

    const {isLoaded, loadError} = useLoadScript({
        googleMapsApiKey: 'AIzaSyA5Xq7Slz1vcvRu1U6dhcCuljEwl84bE7Y'
    });

    if (loadError) return (<p>Error loading Google Maps!</p>);
    if (!isLoaded) return (<p>Loading Google Maps!</p>);

    let center = {
        lat: props.userLocation.latitude,
        lng: props.userLocation.longitude
    }

    const markers = props.foodTrucks.map(foodTruck => {
        const position = {lat: foodTruck.location.coordinates[1], lng: foodTruck.location.coordinates[0]}
        const icon = {url: '/images/food-truck.png', scaledSize: new window.google.maps.Size(30, 30)};
        return (
            <Marker key={foodTruck.objectid} title={foodTruck.applicant} position={position} icon={icon}
                    onClick={() => {
                        setSelected(foodTruck);
                    }}/>

        );
    });

    console.log('selected', selected);

    const infoWindow = selected ?
        <InfoWindow key={selected.objectid} position={{lat: selected.location.coordinates[1], lng: selected.location.coordinates[0]}}>
            <div>
                <h6>{selected.applicant}</h6>
                <p><strong>{selected.address}</strong></p>
                <a href={selected.schedule} target="_blank">Schedule</a>
                <p><strong>Food:</strong>{selected.fooditems}</p>
            </div>
        </InfoWindow> : null;

    console.log('InfoWindow', infoWindow);

    return (
        <div className="row offset-md-4">
            <GoogleMap
                center={!selected ? center : {lat: selected.location.coordinates[1], lng: selected.location.coordinates[0]}}
                mapContainerStyle={mapContainerStyle} zoom={2} options={options}>
                <Marker title="You are here!" position={center}/>
                {markers}
                {infoWindow}
            </GoogleMap>
        </div>
    );
}

export default MapContainer;
