import * as ActionTypes from "./ActionTypes"
import {CustomAction} from "./Action";
import FoodTruck from "../components/Model/FoodTruck";

export interface FoodTrucksReducerState {
    isLoading: boolean,
    errorMessage: string,
    foodTrucks: Array<FoodTruck>
}

export const FoodTrucksReducer = (state: FoodTrucksReducerState = {
    isLoading: false,
    errorMessage: "",
    foodTrucks: []
}, action: CustomAction) => {
    switch (action.type) {
        case ActionTypes.GET_FOOD_TRUCKS_INFORMATION:
            return {...state, isLoading: false, errorMessage: "", foodTrucks: action.payload}
        case ActionTypes.FOOD_TRUCKS_INFORMATION_LOADING:
            return {...state, isLoading: true, errorMessage: "", foodTrucks: []}
        case ActionTypes.FOOD_TRUCKS_LOAD_FAILED:
            return {...state, isLoading: false, errorMessage: action.payload.toString(), foodTrucks: []}
        default:
            return state;
    }
}