import * as ActionTypes from "./ActionTypes";
import {CustomAction} from "./Action";
import {ThunkDispatch} from "redux-thunk";

export const saveLocation = (latitude: number, longitude: number): CustomAction => ({
    type: ActionTypes.GET_USER_LOCATION,
    payload: JSON.stringify({latitude, longitude})
})

export const fetchUserLocation = () => (dispatch: ThunkDispatch<any, any, any>) => {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(userLocation => {
            dispatch(saveLocation(userLocation.coords.latitude, userLocation.coords.longitude));
        });
    }
}

export const addFoodTrucks = (foodTrucks: string) => ({
    type: ActionTypes.GET_FOOD_TRUCKS_INFORMATION,
    payload: foodTrucks
})

export const foodTrucksLoading = () => ({
    type: ActionTypes.FOOD_TRUCKS_INFORMATION_LOADING,
    payload: ""
})

export const foodTrucksFailed = (errorMessage: string) => ({
    type: ActionTypes.FOOD_TRUCKS_LOAD_FAILED,
    payload: errorMessage
})

export const fetchFoodTrucks = () => (dispatch: ThunkDispatch<any, any, any>) => {
    fetch('https://data.sfgov.org/resource/rqzj-sfat.json')
        .then(response => {
            if (response.ok) {
                return response;
            } else {
                throw new Error('Error ' + response.status + ': ' + response.statusText);
            }
        }, error => {
            throw new Error(error.message);
        })
        .then(response => response.json())
        .then(foodTrucks => dispatch(addFoodTrucks(foodTrucks)))
        .catch(error => dispatch(foodTrucksFailed(error)))
}

