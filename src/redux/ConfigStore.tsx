import {applyMiddleware, combineReducers, createStore} from "redux";
import thunk from "redux-thunk";
import {logger} from "redux-logger";
import {UserLocationReducer} from "./UserLocationReducer";
import {FoodTrucksReducer} from "./FoodTrucksReducer";

export const ConfigStore = () => {
    return createStore(combineReducers({
        userLocation: UserLocationReducer,
        foodTrucks: FoodTrucksReducer
    }), applyMiddleware(thunk, logger));
}