import * as ActionTypes from "./ActionTypes"
import {CustomAction} from "./Action";

export interface UserLocationReducerState {
    latitude: number,
    longitude: number
}

export const UserLocationReducer = (state: UserLocationReducerState = {
    latitude: 0,
    longitude: 0
}, action: CustomAction) => {
    switch (action.type) {
        case ActionTypes.GET_USER_LOCATION:
            const location = JSON.parse(action.payload.toString());
            return {...state, latitude: location.latitude, longitude: location.longitude}
        default:
            return state;
    }
}