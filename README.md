This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

# Welcome to FoodTrucks

Food Trucks is a web development exercise, which
consist in load Food Trucks information from [data.sfgov.org](https://data.sfgov.org/Permitting/Mobile-Food-Facility-Permit/rqzj-sfat).
Then using Google Maps Javascript API, well, using it implementation on the library [@react-google-maps](https://www.npmjs.com/package/@react-google-maps/api).
The geological information from each food truck is render in the map as a marker with a custom icon. Also, if the user allow it, his or her location
will be render in the map. When any marker is clicked, the map will render a window with further information regarding the food truck.

## `Tech Stack`

This website was build with [React JS](https://reactjs.org). Implementing [Redux](https://redux.js.org) architecture.

* [Typescript](https://www.typescriptlang.org)
* [React](http://reactjs.org/)
* [React-Router](https://reactrouter.com/web/guides/quick-start)
* [Redux](https://redux.js.org)
* [Redux-Thunk](https://github.com/reduxjs/redux-thunk)
* [React-Google-Maps](https://tomchentw.github.io/react-google-maps/)
* [Reactstrap](https://reactstrap.github.io)
* [Fontawesome](https://github.com/FortAwesome/react-fontawesome)

## `Architecture`

This web application was created with [Create React App](https://github.com/facebook/create-react-app).
So it has the default structure from this. Inside the source folder, I create a component folder to store
every custom component I would need for the application, also, I create a redux folder to store redux 
implementation components. 

### Redux Components

* Action: Custom action interface to use among the reducers.
* ActionTypes: Here you will find the different action types defined for the application.
* ActionCreator: Stores the different Actions that the application use to change the state.
* ConfigStore: Here you will find the storage of the states of each used reducer.
* UserLocationReducer: As the name says, this reducer is in charge of everything regarding the user location.
* FoodTrucksReducer: As the name says, this reducer is in charge of everything regarding the food trucks information.

### Model Componets

* FoodTruck: this component is a class tha describes the food truck json structure.

### Components

* Main: This component is the director of the orchestra, is the one triggering the different actions of the application,
 the one managing the flow of the information throw all the other components of the application.
* Header: Allocate the top navigation bar with the menu.
* Footer: Allocate the bottom menu of the page, where you find navigation links, information of the business and social network links.
* Home: It is the default component, contain the welcome message, the logo and the map component.
* MapComponent: Here is where the magic happens, you will find the [React-Google-Maps](https://tomchentw.github.io/react-google-maps/) implementation here.
* AboutUs: Here you will find a breve description of the website, why it exists, the technology used and Me on the credits, I need to be in it, I build it, so.

**Note: this application has the default commands `start`, `build`, `test` and `eject`. you can check package.json! for more information.**

This was fun to build!
